## Limiting bandwidth per process user o group ##

Here is a short command list to limit the upload bandwidth for a specific user.    
Lines that begins with ```#```: commands to be run as root    
Lines that begins with ```$```: commands to be run as unprivileged user

TC queue:

```
tc qdisc  add dev <INTERFACE> root handle 1: htb default 15
tc class  add dev <INTERFACE> parent 1: classid 1:1 htb rate <LIMIT>kbit ceil <LIMIT>kbit prio 1
tc filter add dev <INTERFACE> parent 1:0 protocol ip prio 1 handle 10 fw classid 1:1
```

Iptables rules (eg. 443/TCP):

```
iptables -t mangle -A OUTPUT -p <PROTOCOL> --dport <PORT> -m owner --uid-owner <UID> -j MARK --set-mark 0xa
```

### IRL example ###

We're going to limit upload rate for ```atomicwallet``` (known to be upload hungry to some servers on 443/TCP).

Create a dedicated system user (eg. ```atomfifty```):

```
# adduser atomfifty
```

Get the UID fo that user:

```
# id -u atomfifty
1001
```

TC/iptable rules:

```
# tc qdisc  add dev eth0 root handle 1: htb default 15
# tc class  add dev eth0 parent 1: classid 1:1 htb rate 50kbit ceil 50kbit prio 1
# tc filter add dev eth0 parent 1:0 protocol ip prio 1 handle 10 fw classid 1:1
# iptables -t mangle -A OUTPUT -p tcp --dport 443 -m owner --uid-owner 1001 -j MARK --set-mark 0xa
```

Allow local connections to our X server:

```
$ xhost +local:localhost
```

Become the atomic user:

```
$ su - atomfifty
```

Set the DISPLAY variable:

```
$ export DISPLAY=:0.0
```
 
Run atomicwallet:

```
$ atomicwallet
```

In another shell we can fire up ```nethogs``` or ```iftop``` to verify the upload rates.

As an alternative, you can try the iptables module ```--pid-owner``` with the
PID of the misbehaving process, with no need of dedicated user.
